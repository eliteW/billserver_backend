
from django.contrib import admin
from django.urls import path
from django.conf.urls import  include, url
from django.conf import settings
from django.template import RequestContext
from django import template
from django.template import Context
from rest_framework.schemas import get_schema_view
from api import views
from multilogin import multilogin

urlpatterns = [
    url(r'^startLogin/$', views.startLogin, name="startLogin"),
    url(r'^opt/$', views.opt, name="opt"),
    url(r'^phoneNumber/$', views.phoneNumber, name="phoneNumber"),
    url(r'^makePayment/$', views.makePayment, name="makePayment"),
    url(r'^paymentAmount/$', views.paymentAmount, name="paymentAmount"),
    url(r'^paymentDate/$', views.paymentDate, name="paymentDate"),
    url(r'^paymentDateBack/$', views.paymentDateBack, name="paymentDateBack"),
    url(r'^paymentMethodBack/$', views.paymentMethodBack, name="paymentMethodBack"),
    url(r'^paymentMethod/$', views.paymentMethod, name="paymentMethod"),
    url(r'^getBill/$', views.getBill, name="getBill"),
    url(r'^deleteBill/$', views.deleteBill, name="deleteBill"),
    url(r'^refreshLogin/$', views.refreshLogin, name="refreshLogin"),
]