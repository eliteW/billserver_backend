
import os
import logging
from django.shortcuts import render, redirect
from django.template import RequestContext
from datetime import datetime, date
from django.contrib.auth.decorators import permission_required
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.decorators import login_required

from django.http import HttpResponse, JsonResponse
from django.contrib.auth.models import User, Group
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
import json
import hashlib
import random
from time import sleep

from rest_framework import status
from rest_framework.generics import (ListCreateAPIView,
                                     RetrieveAPIView)
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from django.forms.models import model_to_dict
from django.core.mail import send_mail
from django.template import loader
from multilogin.multilogin import MultiLogin
from multilogin.sites.att.attInitialLogin import AttLogin
from multilogin.sites.att.attRefreshLogin import RefreshLogin
from multilogin.sites.att.attPayment import AttMakePayment
from multilogin.sites.spectrum.spectrum import Spectrum
from multilogin.sites.consumersenergy.consumersenergy import Consumersenergy
# from multilogin.multilogin_manager import MultiloginManager
from multilogin.get_bill import get_bill

def startLogin(request):
    tem = "Ok"
    print(tem)
    if request.method == "POST":
        userId = request.POST["userId"]
        password = request.POST["password"]
        siteUrl = request.POST["siteUrl"]
    result = {}
    # ml = MultiLogin()
    # (temp, date, amount) = ml.process(userId, password, phone_number)
    # result["res"] = temp
    # result["billDate"] = date
    # result["amount"] = amount

    # ml = MultiloginManager()
    # ml.create_profile("testest", "13.92.168.44", "1080", "df","ss")
    # ml.remove_profile()
    if siteUrl == "att.com":
        print("53535353535353")
        att = AttLogin()
        (temp, phone_number, uuid) = att.startConnection(userId, password, siteUrl)
        # att = AttMakePayment()
        # (temp, phone_number, uuid) = att.getInfoFromDB(userId, password, siteUrl)
        # print(temp)
    elif siteUrl == "spectrum.net":
        print("spectrun called")
        ml = Spectrum()
        (temp, date, amount, uuid) = ml.startConnection(userId, password, siteUrl)
    elif siteUrl == "consumersenergy.com":
        print("consuermer called")
        consumer = Consumersenergy()
        (temp, date, amount, uuid) = consumer.startConnection(userId, password, siteUrl)
    # print(res["billDate"])
    # print(res['amount'])
    print("555555555555views")

    # print(temp, date, amount,uuid)
    # result["res"] = temp
    # result["billDate"] = date
    # result["amount"] = amount
    # result["uuid"] = uuid
    # print(temp)

    result['res'] = temp
    result['phone_number'] = phone_number
    result['uuid'] = uuid
    return JsonResponse(result, safe=False)

def refreshLogin(request):
    if request.method == "POST":
        userId = "4196511828"
        password = "T54dbTF67!"
        siteUrl = request.POST["siteUrl"]
    if siteUrl == "att.com":
        print("53535353535353")
        print("started")
        att = RefreshLogin()
        (temp, phone_number, uuid) = att.startConnection(userId, password, siteUrl)
        # att = AttMakePayment()
        # (temp, phone_number, uuid) = att.getInfoFromDB(userId, password, siteUrl)
        # print(temp)
    result = {}
    result["res"] = temp
    result["billDate"] = date
    result["amount"] = amount
    return JsonResponse(result, safe=False)

def opt(request):
    if request.method == "POST":
        code = request.POST['code']
        uuid = request.POST['uuid']
        username = request.POST['username']
    mlOpt = AttLogin()
    (temp,date,amount) = mlOpt.initialConnectionFromOtp(uuid, code, username)
    result = {}
    result["res"] = temp
    result["billDate"] = date
    result["amount"] = amount
    return JsonResponse(result, safe=False)

def phoneNumber(request):
    if request.method == "POST":
        which_phone_number = request.POST['phoneNumber']
        uuid = request.POST['uuid']
    print("phoneNumber is called")
    print(uuid)
    print(which_phone_number)
    mlPhoneNumber = AttLogin()
    (res, uuid) = mlPhoneNumber.initialConnectionPhoneNumber(uuid,which_phone_number)
    result = {}
    result["res"] = res
    result['uuid'] = uuid

    return JsonResponse(result, safe=False)

# def makePayment(request):
#     if request.method == "POST":
#         uuid = request.POST['uuid']
#     print("phoneNumber is called")
#     print(uuid)
#     mlSpectrum = Spectrum()
#     (res, uuid) = mlSpectrum.makePayment(uuid)
#     result = {}
#     result["res"] = res
#     result['uuid'] = uuid

#     return JsonResponse(result, safe=False)

def makePayment(request):
    print("paymentddddd")
    if request.method == "POST":
        siteUrl = request.POST['site']
        amount = request.POST['amount']
        pay_date = request.POST['pay_date']
        card_number = request.POST["card_number"]
        expiration_date = request.POST['expiration_date']
        security_code = request.POST['security_code']
        print(amount,"dddddddddddddddddddddddddddd")
        print(security_code)

    if siteUrl == "att.com":
        print("att site inside called")
        attPayment = AttMakePayment()
        (temp, phone_number, uuid) = attPayment.getInfoFromDB(amount, pay_date, card_number, expiration_date, security_code, siteUrl)
        print(temp)
    result = {}
    result["res"] = res
    result['uuid'] = uuid

    return JsonResponse(result, safe=False)

def paymentAmount(request):
    if request.method == "POST":
        amount = request.POST['amount']
        uuid = request.POST['uuid']
    mlSpectrum = Spectrum()
    (res, uuid, today, otherDate) = mlSpectrum.paymentAmount(uuid, amount)
    result = {}
    result["res"] = res
    result['uuid'] = uuid
    result["todayDate"] = today
    result["otherDate"] = otherDate

    return JsonResponse(result, safe=False)

def paymentDateBack(request):
    if request.method == "POST":
        uuid = request.POST['uuid']
    mlSpectrum = Spectrum()
    (res, uuid) = mlSpectrum.paymentDateBack(uuid)
    result = {}
    result["res"] = res
    result['uuid'] = uuid

    return JsonResponse(result, safe=False)

def paymentDate(request):
    if request.method == "POST":
        b_todayDate = request.POST['todayDate']
        otherDate = request.POST['otherDate']
        uuid = request.POST['uuid']
    mlSpectrum = Spectrum()
    print("payment date here",b_todayDate)
    print(uuid)
    (res, uuid) = mlSpectrum.paymentDate(uuid, b_todayDate, otherDate)
    result = {}
    result["res"] = res
    result['uuid'] = uuid

    return JsonResponse(result, safe=False)

def paymentMethodBack(request):
    if request.method == "POST":
        uuid = request.POST['uuid']
    mlSpectrum = Spectrum()
    (res, uuid) = mlSpectrum.paymentDateBack(uuid)
    result = {}
    result["res"] = res
    result['uuid'] = uuid

    return JsonResponse(result, safe=False)

def paymentMethod(request):
    print("payment methoddddd")
    if request.method == "POST":
        method = request.POST['status']
        first = request.POST['first']
        second = request.POST['second']
        uuid = request.POST['uuid']
    mlSpectrum = Spectrum()
    print("payment method here", method)
    print(uuid)
    (res, uuid) = mlSpectrum.paymentMethod(uuid, method, first, second)
    result = {}
    result["res"] = "res"
    result['uuid'] = "uuid"

    return JsonResponse(result, safe=False)

    # #Test vs.net
    # print("goodgood")
    # test = Test()
    # test.startConnection("chouser1160@gmail.com", "Solar480","http")

# def saveBill(request):
#     print("laid le hahahha")
#     if request.method == "POST":
#         site_name = request.POST['url']
#     print(site_name)  
#     res = save_bill(site_name)
#     print("181818181818181", res)
#     result = {}
#     result["res"] = "res"

#     return JsonResponse(result, safe=False)

def getBill(request):
    print("195195195195")
    res = get_bill()
    print("181818181818181", res)

    return JsonResponse(res, safe=False)

def deleteBill(request):
    return 0
