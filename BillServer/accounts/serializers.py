from rest_auth.registration.serializers import RegisterSerializer
from rest_framework import serializers

from accounts.models import Account


class RegisterAccountSerializer(RegisterSerializer):
    
    firstname = serializers.CharField(
        allow_blank=True,
        label="First name"
    )
    lastname = serializers.CharField(
        allow_blank=True,
        label="Last name"
    )

    def custom_signup(self, request, user):
        
        user.firstname = self.validated_data.get('firstname')
        user.lastname = self.validated_data.get('lastname')
        user.save()


class AccountSerializer(serializers.ModelSerializer):

    class Meta:
        model = Account
        fields = ('firstname', 'lastname')
