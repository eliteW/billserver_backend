import os
from django.db import models
from django.contrib.auth.models import User, Group
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.utils import timezone

from accounts.managers import AccountManager

# Create your models here.

class Account(AbstractBaseUser, PermissionsMixin):

    firstname = models.CharField(max_length=255)
    lastname = models.CharField(max_length=255)
    email = models.EmailField(unique=True, null=True, db_index=True)

    is_active = models.BooleanField('active', default=True)

    date_joined = models.DateTimeField('date joined', default=timezone.now)

    USERNAME_FIELD = 'email'

    objects = AccountManager()

    @property
    def is_staff(self):
        return self.is_superuser
