var result = '##result##';
var username = '##username##';
var password = '##password##';

document.getElementById("g-recaptcha-response").style.display = "block";
document.getElementById('g-recaptcha-response').innerHTML = result;

console.log(result);
var data = JSON.stringify({
	"Username": username,
	"Password": password,
	"keepMeIn": true,
	"captcha": result,
	"TargetURL": "https://www.spectrum.net/billing-and-transactions/?oneTimePayment=true",
	"AttemptNumber": 1,
	"g-captcha-response": result,
	"PastAuthValues": ""
});
console.log(data);

const headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
};
fetch(`https://www.spectrum.net/api/pub/authn/v1/authenticate?sourcePage=standalone`, {
    headers: headers,
    body: data,
    method: 'POST'
}).then(response => response.json()).then(res => {
	console.log(res);
});
