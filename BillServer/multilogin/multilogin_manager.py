import requests
import json
from time import sleep
# from pandas.io.json import json_normalize
import random
from datetime import datetime

class MultiloginManager:
    def __init__(self):
        self.auth_token = "979f18a2cc95c1be8c383b1280af15064f3b69ae"
        self.profile_list_url = "https://api.multiloginapp.com/v1/profile/list"
        self.group_list_url = "https://api.multiloginapp.com/v1/tag/list"
        self.profile_remove_url = "https://api.multiloginapp.com/v1/profile/remove"
        self.profile_create_url = "https://api.multiloginapp.com/v2/profile?token=979f18a2cc95c1be8c383b1280af15064f3b69ae&mlaVersion=4.5.3"
        
    def create_profile(self, profile_name, ip_address, port, lat, lng, username, password):
        print("test started gagagagagaga")
        print(port)
        data = {
                "name": profile_name,
                "browser": "mimic",
                "os": "win",
                "googleServices": True,
                "network": {
                    "proxy": {
                        "type": "HTTP",
                        "host": ip_address,
                        "port": port,
                        "username": username,
                        "password": password
                        }
                    },
                # "geolocation": {
                #     "mode": "propmt",
                #     "fillBasedOnExternalIp": False,
                #     "lat": lat,
                #     "lng": lng,
                #     "accuracy": 100
                # },
                }
        response = requests.post(url = self.profile_create_url,
            headers = {"Content-Type": "application/json"},
            json = data
            )
        return response.json()

    def remove_profile(self, profileUuid):
        PARAMS = {"token": self.auth_token, "profileId": profileUuid}
        response = requests.get(self.profile_remove_url, PARAMS)
        print("profile deleted")

    def get_profile_data(self, profileUuid):
        url = "https://api.multiloginapp.com/v1/profile/get-data"
        PARAMS = {"token": self.auth_token, 'profileId': profileUuid}
        response = requests.get(url, PARAMS)
        data = response.json()
        return data
