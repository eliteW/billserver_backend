from selenium import webdriver
from selenium.webdriver.firefox import options
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import requests
from time import sleep
from random import randint
from multilogin.models import (ProfileInfo)
from multilogin.models import (SiteCredential)
from multilogin.models import (ProfileName)
from multilogin.models import (EncryptedInfoField, UsernameVault, PasswordVault, CardInfoVault)
from multilogin.multilogin_manager import MultiloginManager
from multilogin.constants import (DO_INITIAL_CONNECTION, INVALID_PWD, INVALID_CREDENTIAL,
                        INVALID_OTP, BALANCE_ERROR, REFRESHCONNECTION_ERROR,
                        INITIALCONNECTION_ERROR, GET_PHONENUMBER_ERROR, PHONE_NUMBER_LOADING_ERROR,
                        OTP_CODE, OTP_LOADING_ERROR, GET_BALANCE_SUCCESS, PAYMENT_AMOUNT_ERROR, PAYMENT_DATE_ERROR, PAYMENT_METHOD_ERROR, PAYMENT_COMPLETE_ERROR)
from encryption.encrypt_decrypt import (transit_encrypt, transit_decrypt)
from recaptcha.recaptcha import (recaptcha, antiCaptcha)
from anticaptchaofficial.recaptchav3proxyless import *

class Spectrum:
    def __init__(self):
        self.driver = ""
        self.mla_profile_uuid = ""
        self.phone_number = ""
        self.password = ""
        self.new_password = ""
        self.profile_uuid = ""

    def startProfile(self):  
        print(self.profile_uuid)
        mla_url = 'http://127.0.0.1:9000/api/v1/profile/start?automation=true&profileId=' + self.profile_uuid
        
        """
        Send GET request to start the browser profile by profileId. Returns response in the following format: '{"status":"OK","value":"http://127.0.0.1:XXXXX"}', where XXXXX is the localhost port on which browser profile is launched. Please make sure that you have Multilogin listening port set to 35000. Otherwise please change the port value in the url string
        """
        resp = requests.get(mla_url)

        json = resp.json()
        print(f'mutlilogin json: {json}')
        print(json)
        print(json['status'])
        print("this is the end of this one")
        #Define DesiredCapabilities
        opts = options.DesiredCapabilities()

        #Instantiate the Remote Web Driver to connect to the browser profile launched by previous GET request
        self.driver = webdriver.Remote(command_executor=json['value'], desired_capabilities={})
        ################## this is to restore the profile browser after otp and phone number input ######
        profile_info = ProfileInfo.objects.filter(profile_uuid=self.profile_uuid)[0]
        profile_info.profile_json = json['value']
        profile_info.save()
        ################################################################################################

    def BinaryToDecimal(self, binary):  
            
        binary1 = binary  
        decimal, i, n = 0, 0, 0
        while(binary != 0):  
            dec = binary % 10
            decimal = decimal + dec * pow(2, i)  
            binary = binary//10
            i += 1
        return (decimal)  
    
    def startConnection(self, userId, password, siteUrl):
        print("okokokokokok")
        ################################# check data base with userId  ##############################
        ################ decrypt all the info to get the username ##############
        usernameVault = UsernameVault.objects.all()
        b_userId_exist = False
        for eachUsername in usernameVault:
            print(eachUsername.username_key, eachUsername.cipherUsername)
            userName = transit_decrypt(eachUsername.cipherUsername, eachUsername.username_key)
            if (userName == userId):
                b_userId_exist = True
                userName_row = eachUsername
                break
        ################## End ###############################################
        if (b_userId_exist):  # if userid exists should open existing profile browser
            print("exddddddddddddddddddddddddddddddddddddddddddddddddddddddddist")
        ############### get profile uuid #####################
            # site_credential =SiteCredential.objects.filter(userId=userId)[0]
            # self.profile_uuid = site_credential.profile.profile_uuid
            print("848484848484 this is it")
            encryptedInfoField = EncryptedInfoField.objects.filter(userName=userName_row)[0]
            self.profile_uuid = encryptedInfoField.profile.profile_uuid
            print(self.profile_uuid)
        ######################################################
        ################ open profile browser ##################
            self.startProfile()
            sleep(10)
            (temp, billDate, balance) = self.connection(userId, password, siteUrl)
            return (temp, billDate, balance, self.profile_uuid)
        ##########################################################
            # (temp, billDate, balance) = self.refreshConnection(userId, password, siteUrl)
            # if (temp == INVALID_PWD):  # when invalid password, ask initial connection (delete profile browser)
            #     ml_manager = MultiloginManager()
            #     ml_manager.remove_profile(self.profile_id)
            #     site_credential =SiteCredential.objects.filter(userId=userId)[0]
            #     site_credential.delete()
            #     profile_info = site_credential.profile_id
            #     profile_info.delete()
            #     return (DO_INITIAL_CONNECTION, "", "")
            # return (temp, billDate, balance)
        else:
            print("not existing  called")
        ##################### Handle profilename in the database #################
            profile_name_num = ProfileName.objects.all()
            if (len(profile_name_num) == 0):   # when there is no value, input 1 as a default
                profile_name_temp = ProfileName(lastProfileNumber=1)
                profile_name_temp.save()
            current_profile_name = ProfileName.objects.all()[0].lastProfileNumber + 1
            profile_name = "profile" + str(current_profile_name)
        ############################################################################
        ##################### create new profile browser // save info in the database // open profile browser ##############
            ml_manager = MultiloginManager()
            profile_response = ml_manager.create_profile(profile_name, "gate.dc.smartproxy.com", "20001", "lat", "lng", "spa5aef9ac", "Swqa1234+")  # create profile browser
            print("response here", profile_response['uuid'])
            # save profile browser info in the database
            profile_browser_info = ProfileInfo(profile_name=profile_name, profile_uuid=profile_response['uuid'], ip_address="ip_address", port=123, lat=00, lng=11)
            profile_browser_info.save()
            self.profile_uuid = profile_response['uuid']
            #self.profile_id = "86c4ca33-46d3-43a2-aadf-4a3f41866d0f"
            sleep(1)
        #########################################################################################
            self.startProfile()
            sleep(10)
            (temp, billDate, balance) = self.connection(userId, password, siteUrl)
            ############################# save site credentials into database when it is success ##############
            if (temp == "success"):
                print(102102102102102)
                site_credential = SiteCredential(userId=userId, password=password, profile=profile_browser_info, site_url=siteUrl)
                site_credential.save()
                ###### Encrypt info and save in the database #############
                username_key = "username" + str(current_profile_name)
                encryptedUserName = transit_encrypt(userId, username_key)
                password_key = "password" + str(current_profile_name)
                encryptedPassword = transit_encrypt(password, password_key)
                username_db_field = UsernameVault(username_key=username_key, cipherUsername=encryptedUserName)
                username_db_field.save()
                password_db_field = PasswordVault(password_key=password_key, cipherPassword=encryptedPassword)
                password_db_field.save()
                encryptedInfo = EncryptedInfoField(site_url=siteUrl, profile=profile_browser_info, userName=username_db_field, password=password_db_field)
                encryptedInfo.save()
                ####################  END  ###############################
            return (temp, billDate, balance, self.profile_uuid)
    
    def connection(self, userId, password, siteUrl):
        # try:
        siteUrl = "https://www." + siteUrl + "/login"
        print(siteUrl)
        self.driver.get(siteUrl)
        sleep(7)
        username = self.driver.find_element_by_xpath('//*[@id="cc-username"]') 
        username.send_keys(userId)
        sleep(2)
        pwd = self.driver.find_element_by_xpath('//*[@id="cc-user-password"]')
        pwd.send_keys(password)
        sleep(2)
        print("captchadklafjdklasfjdklasjfdkl")
        self.captcha(userId, password)
        # try: 
        #     captcha_element = self.driver.find_element_by_xpath('//iframe')
        #     print("dfdsfdsddddddddddddddd")
        #     self.recaptcha()
        # except:
        #     print("Does not require recaptcha")
        # sleep(15)
        print("dfasfdasfdsafasgas")
        # self.driver.get("https://www.spectrum.net/")
        # signin_button = self.driver.find_element_by_xpath('//*[@id="shared-login-container"]/form/button')
        # signin_button.click()
        print("121121121121")
        sleep(3)
        try:
            username = self.driver.find_element_by_xpath('//*[@id="cc-username"]') 
            return (INVALID_CREDENTIAL, "", "")
        except:
            (temp,date,amount) = self.getBalance()
            print("ddddd", temp)
            return (temp, date, amount)          
        # except:
        #     print("this is what we want")
        #     return (CONNECTION_ERROR, "", "")

    def captcha(self, userId, password):
        print("uuid", self.profile_uuid)
        # try:
        print("187187187187")
        sleep(1)
        captcha_element = self.driver.find_element_by_xpath('//iframe')
        print("189189189")
        temp = captcha_element.get_attribute("src")
        print("srcsrcsrscsrc")
        key = temp.split("&k=")[1].split("&co=")[0]
        print("keykeykey", key)
        print("187187181718171817")
        self.driver.execute_script("console.log('javascrupted called')")
        response = recaptcha(key, userId, password)
        # temp = response.split("OK|")[1]
        # print("20020202", temp)
        # sleep(1)
        print("205025025")
        print("ddddddddddd", response)
        self.driver.execute_script(response)

        # self.driver.maximize_window()
        # self.driver.execute_script("document.getElementById('g-recaptcha-response').style.display = 'block';")
        # sleep(2)
        # self.driver.execute_script("document.getElementById('g-recaptcha-response').innerHTML='"+response+"';")
        # self.driver.execute_script("console.log('input response in textarea')")
        # sleep(1)
        # self.driver.switch_to.frame(self.driver.find_element_by_xpath('//*[@id="shared-login-container"]/form/captcha/div/div/div/div[2]/div/div/div/iframe'))
        # radio = self.driver.find_element_by_xpath('//*[@id="recaptcha-anchor"]/div[1]')
        # radio.click()
        # self.driver.switch_to.default_content()
        # textarea = self.driver.find_element_by_xpath('//*[@id="g-recaptcha-response"]')
        # # textarea.send_keys(response)
        # self.driver.execute_script("arguments[0].value = arguments[1]", textarea, response)
        sleep(5)


    # def readJS(self, response, userId, password):
    #     print("228228282828282828282")
    #     print("called hree")
    #     f = open("login.js", "r")
    #     lines = f.readlines()
    #     print("read logodofodsofds")
    #     response = lines[0].replace("##result##",response)
    #     username = lines[1].replace("##username##", userId)
    #     pwd = lines[2].replace("##password##", password)
    #     jsString = response + username + pwd
    #     for x in range(3, len(lines)):
    #         jsString += lines[x]   
    #     print(jsString)
    #     return jsString

    def recaptcha(self):
        sleep(1)
        captcha_element = self.driver.find_element_by_xpath('//iframe')
        print("189189189")
        temp = captcha_element.get_attribute("src")
        print("srcsrcsrscsrc")
        key = temp.split("&k=")[1].split("&co=")[0]
        print("11111111111111111111")
        response = antiCaptcha(key)
        sleep(10)
        print("24224224224224224224")
        self.driver.maximize_window()
        self.driver.execute_script("document.getElementById('g-recaptcha-response').style.display = 'block';")
        sleep(2)
        self.driver.execute_script('document.getElementById("g-recaptcha-response").innerHTML = "%s"' % response)
        self.driver.execute_script("console.log('input response in textarea')")
        sleep(2)
        print("2582582157")
        try:
            submit = self.driver.find_element_by_xpath('//button[@type="submit" and @class="btn-std"]')
            submit.click()
            self.driver.execute_script("console.log('submit button clicked')")
        except:
            print("can not find submit button")

        # textarea = self.driver.find_element_by_xpath('//*[@id="g-recaptcha-response"]')
        # textarea.send_keys(response)
        sleep(10)

    def getBalance(self):
        try:
            sleep(7)
            print("getBalance here")       
            payDate_element = self.driver.find_element_by_xpath('//*[@id="billing-summary-container"]/spectrum-dynamic-fixed-card/div/div[4]/div[2]/div[2]')
            sleep(1)
            print("306306")
            payDate = payDate_element.text
            print(payDate)
            sleep(1) 
            # balance_element_integer = self.driver.find_element_by_xpath('//*[@id="myBilling"]/div[1]/div[1]/div[2]/span/span[2]/span[2]')
            # balance_element_decimal = self.driver.find_element_by_xpath('//*[@id="myBilling"]/div[1]/div[1]/div[2]/span/span[2]/span[3]')
            # sleep(1)     
            # balance = balance_element_integer.text + balance_element_decimal.text   
            balance = self.driver.find_element_by_xpath('//*[@id="billing-summary-container"]/spectrum-dynamic-fixed-card/div/div[4]/div[2]/div[3]/div[1]/div/span[2]').text
            sleep(1)
            print("175175175175175175175")
            print(balance)
            # self.signout()
            return ("success", payDate, balance)
        except:
            print("getBalance error")
            return (BALANCE_ERROR, "","")

    def signout(self):
        sleep(1)
        try:
            signout_button = self.driver.find_element_by_xpath('//*[@id="sign-out"]/span')
            signout_button.click()
            sleep(1)
            print("wowthis is goodreat")
            self.driver.quit()
        except:
            print(SIGNOUT_ERROR)

    def makePayment(self, uuid):
        try:
            self.profile_uuid = uuid
            print("240240240240240240240")
            profile_info = ProfileInfo.objects.filter(profile_uuid=self.profile_uuid)[0]
            self.driver = webdriver.Remote(command_executor=profile_info.profile_json, desired_capabilities={})
            print("243243243243")
            try:
                make_payment_button = self.driver.find_element_by_xpath('//*[@id="billing-summary-container"]/spectrum-dynamic-fixed-card/div/div[4]/div[2]/div[3]/div[2]/div/button')
                make_payment_button.click()
            except:
                print("make payment error")
            return ("success", uuid)
        except:
            print("make payment error")
            return ("fail", uuid)

    def paymentAmount(self, uuid, amount):
        sleep(1)
        self.profile_uuid = uuid
        profile_info = ProfileInfo.objects.filter(profile_uuid=self.profile_uuid)[0]
        self.driver = webdriver.Remote(command_executor=profile_info.profile_json, desired_capabilities={})
        print("186186186186186")
        try:
            amountInput = self.driver.find_element_by_xpath('//*[@id="id"]')
            amountInput.send_keys(amount)
            sleep(1)
            continueButton = self.driver.find_element_by_xpath('//*[@id="spectrum-container"]/main/spectrum-billing/div[3]/div/div[2]/billing-one-time-payment/spectrum-expansion-card/spectrum-card/ngk-typography/ngk-card/div/div[3]/spectrum-expansion-card-content/form/billing-payment-amount/spectrum-accordion-panel/div[2]/spectrum-accordion-panel-action-row/app-button-row/div/button')
            continueButton.click()
            sleep(2)
            ############### get date info to show on front-end ##############
            today_element = self.driver.find_element_by_xpath('//*[@id="spectrum-container"]/main/spectrum-billing/div[3]/div/div[2]/billing-one-time-payment/spectrum-expansion-card/spectrum-card/ngk-typography/ngk-card/div/div[3]/spectrum-expansion-card-content/form/billing-payment-date/spectrum-accordion-panel/div[2]/spectrum-accordion-panel-content/div/span/div[1]/label[1]/span[2]')
            today = today_element.text
            sleep(1)
            other_date_element = self.driver.find_element_by_xpath('//*[@id="spectrum-container"]/main/spectrum-billing/div[3]/div/div[2]/billing-one-time-payment/spectrum-expansion-card/spectrum-card/ngk-typography/ngk-card/div/div[3]/spectrum-expansion-card-content/form/billing-payment-date/spectrum-accordion-panel/div[2]/spectrum-accordion-panel-content/div/span/div[1]/label[2]/span[2]')
            other_date = other_date_element.text
            ####API
            return ("amount_continue", uuid, today, other_date)
        except:
            print(PAYMENT_AMOUNT_ERROR)
            return (PAYMENT_AMOUNT_ERROR, "", "", "")
        
    def paymentDateBack(self, uuid):
        sleep(1)
        self.profile_uuid = uuid
        profile_info = ProfileInfo.objects.filter(profile_uuid=self.profile_uuid)[0]
        self.driver = webdriver.Remote(command_executor=profile_info.profile_json, desired_capabilities={})
        print("20520520")
        try:
            back_button = self.driver.find_element_by_xpath('//*[@id="spectrum-container"]/main/spectrum-billing/div[3]/div/div[2]/billing-one-time-payment/spectrum-expansion-card/spectrum-card/ngk-typography/ngk-card/div/div[3]/spectrum-expansion-card-content/form/billing-payment-date/spectrum-accordion-panel/div[2]/spectrum-accordion-panel-action-row/app-button-row/div/button[1]')
            back_button.click()
            print("208208208208")
            return ("paymentAmountBack", uuid)
        except:
            print("paymentAmountBack Error")
            return ("paymentAmountBack", "")
        
    def paymentDate(self, uuid, todayDate, otherDate):
        sleep(1)
        self.profile_uuid = uuid
        print(type(uuid))
        profile_info = ProfileInfo.objects.filter(profile_uuid=self.profile_uuid)[0]
        self.driver = webdriver.Remote(command_executor=profile_info.profile_json, desired_capabilities={})
        print("2225225225225225225")
        try:
            if (todayDate == "true"):
                print("228228228228")
                today_date_choice = self.driver.find_element_by_xpath('//*[@id="spectrum-container"]/main/spectrum-billing/div[3]/div/div[2]/billing-one-time-payment/spectrum-expansion-card/spectrum-card/ngk-typography/ngk-card/div/div[3]/spectrum-expansion-card-content/form/billing-payment-date/spectrum-accordion-panel/div[2]/spectrum-accordion-panel-content/div/span/div[1]/label[1]/span[1]')
                today_date_choice.click()
                sleep(1)
                print("223223223")
                continue_button = self.driver.find_element_by_xpath('//*[@id="spectrum-container"]/main/spectrum-billing/div[3]/div/div[2]/billing-one-time-payment/spectrum-expansion-card/spectrum-card/ngk-typography/ngk-card/div/div[3]/spectrum-expansion-card-content/form/billing-payment-date/spectrum-accordion-panel/div[2]/spectrum-accordion-panel-action-row/app-button-row/div/button[2]')
                continue_button.click()
                sleep(1)
                return ("date_success", uuid)
            else:
                other_date_choice = self.driver.find_element_by_xpath('//*[@id="spectrum-container"]/main/spectrum-billing/div[3]/div/div[2]/billing-one-time-payment/spectrum-expansion-card/spectrum-card/ngk-typography/ngk-card/div/div[3]/spectrum-expansion-card-content/form/billing-payment-date/spectrum-accordion-panel/div[2]/spectrum-accordion-panel-content/div/span/div[1]/label[2]/span[1]')
                other_date_choice.click()
                sleep(1)
                date_select = self.driver.find_element_by_xpath('//*[@id="other-date-input"]')
                date_select.click()
                sleep(1)
                date_select.send_keys(otherDate)
                sleep(1)
                continue_button = self.driver.find_element_by_xpath('//*[@id="spectrum-container"]/main/spectrum-billing/div[3]/div/div[2]/billing-one-time-payment/spectrum-expansion-card/spectrum-card/ngk-typography/ngk-card/div/div[3]/spectrum-expansion-card-content/form/billing-payment-date/spectrum-accordion-panel/div[2]/spectrum-accordion-panel-action-row/app-button-row/div/button[2]')
                continue_button.click()
                sleep(1)
                return ("date_success", uuid)
        except:
            print(PAYMENT_DATE_ERROR)
            return (PAYMENT_DATE_ERROR, "")

    def paymentMethod(self, uuid, method, first, second):
        print("metohddodofdofudsofudiosaufo")
        print(method)
        sleep(1)
        self.profile_uuid = uuid
        print(uuid)
        profile_info = ProfileInfo.objects.filter(profile_uuid=self.profile_uuid)[0]
        print("262262262262262262262", profile_info)
        self.driver = webdriver.Remote(command_executor=profile_info.profile_json, desired_capabilities={})
        print("264264264264")
        try:
            self.driver.switch_to.frame(self.driver.find_element_by_xpath("//iframe[@src='https://pci.spectrum.net/bill-pay']"))
            if (method == "credit"):
                print("credtittt")               
                cardNumber = self.driver.find_element_by_id('credit-card-input')
                print("2692692692692")
                print(cardNumber)
                cardNumber.click()
                sleep(1)
                cardNumber.send_keys(first)
                print("271271271271")
                expiration_date = self.driver.find_element_by_xpath('//*[@id="credit-card-date"]')
                expiration_date.click()
                print("2232322")
                sleep(1)
                expiration_date.send_keys(second)
                print("277277277277277")
                continue_button = self.driver.find_element_by_xpath('/html/body/app-root/ng-component/div/app-button-row/div/button[2]')
                continue_button.click()
                sleep(1)
            elif (method == "checking"):
                print("checkingddfddf")
                sleep(2)
                checking_account_choice = self.driver.find_element_by_xpath('/html/body/app-root/ng-component/div/app-payment-method-content/div/span/span/span/div/label[2]/span[1]')
                checking_account_choice.click()

                routing_number = self.driver.find_element_by_xpath('//*[@id="routing-number-input"]')
                routing_number.click()
                sleep(1)
                routing_number.send_keys(first)

                account_number = self.driver.find_element_by_xpath('//*[@id="account-number-input"]')
                account_number.click()
                sleep(1)
                account_number.send_keys(second)
                continue_button = self.driver.find_element_by_xpath('/html/body/app-root/ng-component/div/app-button-row/div/button[2]')
                continue_button.click()
                sleep(1)
            elif (method == "savings"):
                print("savingsdfjdksjfd")
                savings_account_choice = self.driver.find_element_by_xpath('/html/body/app-root/ng-component/div/app-payment-method-content/div/span/span/span/div/label[3]/span[1]')
                savings_account_choice.click()

                routing_number = self.driver.find_element_by_xpath('//*[@id="routing-number-input"]')
                routing_number.click()
                sleep(1)
                routing_number.send_keys(first)

                account_number = self.driver.find_element_by_xpath('//*[@id="account-number-input"]')
                account_number.click()
                sleep(1)
                account_number.send_keys(second)
                continue_button = self.driver.find_element_by_xpath('/html/body/app-root/ng-component/div/app-button-row/div/button[2]')
                continue_button.click()
                sleep(1)
            self.driver.switch_to.default_content()
            ### make payment review button ###
            # make_payment_button = self.driver.find_element_by_xpath('//*[@id="spectrum-container"]/main/spectrum-billing/div[3]/div/div[2]/billing-one-time-payment/spectrum-expansion-card/spectrum-card/ngk-typography/ngk-card/div/div[3]/spectrum-expansion-card-content/form/billing-payment-review/spectrum-accordion-panel/div[2]/spectrum-accordion-panel-content/div/div/app-button-row/div/button[2]')
            # make_payment_button.click()
            # self.driver.save_screenshot("paymentscreenshot.png")
            # sleep(1)
            return ("method_success", uuid)
        except:
            print(PAYMENT_METHOD_ERROR)
            return (PAYMENT_METHOD_ERROR, uuid)
    
    def completePayment(self, b_completePayment):
        self.profile_uuid = uuid
        print(type(uuid))
        profile_info = ProfileInfo.objects.filter(profile_uuid=self.profile_uuid)[0]
        self.driver = webdriver.Remote(command_executor=profile_info.profile_json, desired_capabilities={})
        try:          
            if (b_completePayment):
                complete_button = self.driver.find_element_by_xpath('//*[@id="spectrum-container"]/main/spectrum-billing/div[3]/div/div[2]/billing-one-time-payment/spectrum-expansion-card/spectrum-card/ngk-typography/ngk-card/div/div[3]/spectrum-expansion-card-content/form/billing-payment-review/spectrum-accordion-panel/div[2]/spectrum-accordion-panel-content/div/div/app-button-row/div/button[2]')
                complete_button.click()
            else:
                back_button = self.driver.find_element_by_xpath('//*[@id="spectrum-container"]/main/spectrum-billing/div[3]/div/div[2]/billing-one-time-payment/spectrum-expansion-card/spectrum-card/ngk-typography/ngk-card/div/div[3]/spectrum-expansion-card-content/form/billing-payment-review/spectrum-accordion-panel/div[2]/spectrum-accordion-panel-content/div/div/app-button-row/div/button[1]')
                back_button.click()

                ## api call
        except:
            print(PAYMENT_COMPLETE_ERROR)

