from selenium import webdriver
from selenium.webdriver.firefox import options
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import requests
from time import sleep
from random import randint
from multilogin.models import (ProfileInfo)
from multilogin.models import (SiteCredential)
from multilogin.models import (ProfileName, BillInfo)
from multilogin.models import (EncryptedInfoField, UsernameVault, PasswordVault, CardInfoVault)
from multilogin.multilogin_manager import MultiloginManager
from multilogin.constants import (DO_INITIAL_CONNECTION, INVALID_PWD, INVALID_CREDENTIAL,
                        INVALID_OTP, BALANCE_ERROR, REFRESHCONNECTION_ERROR,
                        INITIALCONNECTION_ERROR, GET_PHONENUMBER_ERROR, PHONE_NUMBER_LOADING_ERROR,
                        OTP_CODE, OTP_LOADING_ERROR, GET_BALANCE_SUCCESS, PAYMENT_AMOUNT_ERROR, PAYMENT_DATE_ERROR, PAYMENT_METHOD_ERROR, PAYMENT_COMPLETE_ERROR)
from encryption.encrypt_decrypt import (transit_encrypt, transit_decrypt)
from recaptcha.recaptcha import (recaptcha, antiCaptcha)
from anticaptchaofficial.recaptchav3proxyless import *

class Consumersenergy:
    def __init__(self):
        self.driver = ""
        self.mla_profile_uuid = ""
        self.phone_number = ""
        self.password = ""
        self.new_password = ""
        self.profile_uuid = ""

    def startProfile(self):  
        print(self.profile_uuid)
        mla_url = 'http://127.0.0.1:9000/api/v1/profile/start?automation=true&profileId=' + self.profile_uuid
        
        """
        Send GET request to start the browser profile by profileId. Returns response in the following format: '{"status":"OK","value":"http://127.0.0.1:XXXXX"}', where XXXXX is the localhost port on which browser profile is launched. Please make sure that you have Multilogin listening port set to 35000. Otherwise please change the port value in the url string
        """
        resp = requests.get(mla_url)

        json = resp.json()
        print(f'mutlilogin json: {json}')
        print(json)
        print(json['status'])
        print("this is the end of this one")
        #Define DesiredCapabilities
        opts = options.DesiredCapabilities()

        #Instantiate the Remote Web Driver to connect to the browser profile launched by previous GET request
        self.driver = webdriver.Remote(command_executor=json['value'], desired_capabilities={})
        ################## this is to restore the profile browser after otp and phone number input ######
        profile_info = ProfileInfo.objects.filter(profile_uuid=self.profile_uuid)[0]
        profile_info.profile_json = json['value']
        profile_info.save()
        ################################################################################################
    
    def startConnection(self, userId, password, siteUrl):
        print("okokokokokok")
        profile_name = "profiletest"
    ############################################################################
    ##################### create new profile browser // save info in the database // open profile browser ##############
        ml_manager = MultiloginManager()
        profile_response = ml_manager.create_profile(profile_name, "gate.dc.smartproxy.com", "20001", "lat", "lng", "spa5aef9ac", "Swqa1234+")  # create profile browser
        print("response here", profile_response['uuid'])
        # save profile browser info in the database
        profile_browser_info = ProfileInfo(profile_name=profile_name, profile_uuid=profile_response['uuid'], ip_address="ip_address", port=123, lat=00, lng=11)
        profile_browser_info.save()
        self.profile_uuid = profile_response['uuid']
        #self.profile_id = "86c4ca33-46d3-43a2-aadf-4a3f41866d0f"
        sleep(1)
    #########################################################################################
        self.startProfile()
        sleep(10)
        (status, date, amount) = self.login(userId, password, siteUrl)
        sleep(12)
        ############# save BillInfo ##########
        bill_info, created= BillInfo.objects.get_or_create(username=userId)
        bill_info.site_name = siteUrl
        bill_info.password = password
        bill_info.balance_amount = amount
        bill_info.balance_date = date
        bill_info.profile = profile_browser_info
        bill_info.save()
        ######### End ######
        return ("success",date,amount,self.profile_uuid)

    
    def login(self, userId, password, siteUrl):
        try:
            print(siteUrl)
            self.driver.get("https://www.consumersenergy.com/")
            sleep(7)
            print("9393939393939393993")
            login_menu = self.driver.find_element_by_xpath('/html/body/div[5]/header/div[2]/div/div/div[1]/nav/ul/li[1]/div/button')
            login_menu.click()
            sleep(2)
            print(password)
            username = self.driver.find_element_by_xpath('//*[@id="userLoginDesktop"]') 
            username.send_keys(userId)
            sleep(2)
            pwd = self.driver.find_element_by_xpath('//*[@id="passwordLoginDesktop"]')
            pwd.send_keys(password)
            sleep(2)
            print("time to click button login")
            signin_button = self.driver.find_element_by_xpath('//*[@id="loginButtonDropdown"]')
            signin_button.click()
            sleep(7)
            (status, date, amount) = self.getBalance()
            return (status, date, amount)
            # print("121121121121")
            # try:
            #     username = self.driver.find_element_by_xpath('//*[@id="cc-username"]') 
            #     return (INVALID_CREDENTIAL, "", "")
            # except:
            #     (temp,date,amount) = self.getBalance()
            #     print("ddddd", temp)
            #     return (temp, date, amount)          
        except:
            #delete profile info in the database
            self.removeProfileInfo(self.profile_uuid)
            self.removeProfileSiteCredential(userId)
            print("this is what we want")

    def getBalance(self):
        sleep(5)
        try:
            print("getBalance here")
            payDate_element = self.driver.find_element_by_xpath('//*[@id="currentBalanceAndPlans"]/div[1]/div/div/div/p[4]/strong[2]')
            sleep(2)
            print("306306")
            payDate = payDate_element.text
            print(payDate)
            sleep(1)
            date_element = self.driver.find_element_by_xpath('//*[@id="total-balance"]')
            date = date_element.text
            return (GET_BALANCE_SUCCESS, payDate, date)
        except:
            print("getBalance error")
            self.removeProfileInfo(self.profile_uuid)
            self.removeProfileSiteCredential(userId)
            return (BALANCE_ERROR, "","")

    def signout():
        try:
            sleep(1)
            login_menu = self.driver.find_element_by_xpath('/html/body/div[5]/header/div[2]/div/div/div[1]/nav/ul/li[1]/div/button/span')
            login_menu.click()
            sleep(1)
            logout_element = self.driver.find_element_by_xpath('//*[@id="AccountLoginDetails"]/div[1]/a[2]')
            logout_element.click()
        except:
            self.removeProfileInfo(self.profile_uuid)
            self.removeProfileSiteCredential(userId)
            print("logout error")

    def removeProfileSiteCredential(self, userId):
        ############### delete in the database ###############
        site_credential = SiteCredential.objects.filter(userId=userId)[0]
        site_credential.delete()
        ###################### End ############

    def removeProfileInfo(self, uuid):
        profile_info = ProfileInfo.objects.filter(profile_uuid=uuid)[0]
        profile_info.delete()


# test = Test()
# test.startConnection("chouser1160@gmail.com", "Solar480","http")