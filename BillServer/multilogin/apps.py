from django.apps import AppConfig


class MultiloginConfig(AppConfig):
    name = 'multilogin'
