# Generated by Django 3.0.3 on 2020-03-06 12:10

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('multilogin', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='sitecredential',
            name='profile',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='multilogin.ProfileInfo', verbose_name='RelatedProfile'),
        ),
    ]
