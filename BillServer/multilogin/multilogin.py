from selenium import webdriver
from selenium.webdriver.firefox import options
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import requests
from time import sleep
from random import randint
from multilogin.models import (ProfileInfo, SiteCredential, ProfileName)
from .multilogin_manager import MultiloginManager
from .constants import (DO_INITIAL_CONNECTION, INVALID_PWD, INVALID_CREDENTIAL,
                        INVALID_OTP, BALANCE_ERROR, REFRESHCONNECTION_ERROR,
                        INITIALCONNECTION_ERROR, GET_PHONENUMBER_ERROR, PHONE_NUMBER_LOADING_ERROR,
                        OTP_CODE, OTP_LOADING_ERROR, GET_BALANCE_SUCCESS, PAYMENT_AMOUNT_ERROR)

class MultiLogin:
    def __init__(self):
        self.driver = ""
        self.mla_profile_id = ""
        self.phone_number = ""
        self.password = ""
        self.new_password = ""
        self.profile_id = ""

    def startProfile(self):
        mla_url = 'http://127.0.0.1:9000/api/v1/profile/start?automation=true&profileId=' + self.profile_id
        
        """
        Send GET request to start the browser profile by profileId. Returns response in the following format: '{"status":"OK","value":"http://127.0.0.1:XXXXX"}', where XXXXX is the localhost port on which browser profile is launched. Please make sure that you have Multilogin listening port set to 35000. Otherwise please change the port value in the url string
        """
        resp = requests.get(mla_url)

        json = resp.json()
        print(f'mutlilogin json: {json}')
        print(json)
        print("this is the end of this one")
        #Define DesiredCapabilities
        opts = options.DesiredCapabilities()

        #Instantiate the Remote Web Driver to connect to the browser profile launched by previous GET request
        self.driver = webdriver.Remote(command_executor=json['value'], desired_capabilities={})
################## this is to restore the profile browser after otp and phone number input ######
        # profile_info, created = ProfileInfo.objects.get_or_create(profile_uuid=self.profile_id)
        # profile_info.profile_url = json['value']
        # profile_info.save()

        profile_info = ProfileInfo.objects.filter(profile_uuid=self.profile_id)[0]
        profile_info = ProfileInfo.objects.filter(profile_uuid=self.profile_id)[0]
        profile_info.profile_json = json['value']
        profile_info.save()
################################################################################################

    def startConnection(self, userId, password, site_url):
        siteUrl = "https://www." + site_url
        ################################# check data base with userId  ##############################
        if (SiteCredential.objects.filter(userId = userId).exists()):  # if userid exists should open existing profile browser
            print("exddddddddddddddddddddddddddddddddddddddddddddddddddddddddist")
        ############### get profile uuid #####################
            site_credential =SiteCredential.objects.filter(userId=userId)[0]
            self.profile_id = site_credential.profile.profile_uuid
        ######################################################
        ################ open profile browser ##################
            self.startProfile()
            sleep(10)
        ##########################################################
            (temp, billDate, balance) = self.refreshConnection(userId, password, siteUrl)
            if (temp == BALANCE_ERROR):  # when invalid password, ask initial connection (delete profile browser)
                print("737373737373737373")
                self(1)
                self.driver.quit()
                sleep(3)
                ml_manager = MultiloginManager()
                ml_manager.remove_profile(self.profile_id)
                try:
                    site_credential =SiteCredential.objects.filter(userId=userId)[0]
                    site_credential.delete()
                except:
                    print("delte site credential error")
                try:
                    profile_info = site_credential.profile_id
                    profile_info.delete()
                except:
                    print("delete profile borwser eror")
                self.handleAskInitialConnection(self.profile_id,userId)
                return (DO_INITIAL_CONNECTION, "", "")
            return (temp, billDate, balance)

        else:
            print("not existing  called")
            ##################### Handle profilename in the database #################
            profile_name_num = ProfileName.objects.all()
            if (len(profile_name_num) == 0):   # when there is no value, input 1 as a default
                profile_name_temp = ProfileName(lastProfileNumber=1)
                profile_name_temp.save()
                print("daodi shi shui fan le cuo")
            current_profile_name = ProfileName.objects.all()[0].lastProfileNumber + 1
            profile_name = "profile" + str(current_profile_name)
            profile_name_temp = ProfileName(lastProfileNumber=current_profile_name)
            profile_name_temp.save()
            ##########################################################################
# ##################### create new profile browser // save info in the database // open profile browser ##############
            ml_manager = MultiloginManager()
            profile_response = ml_manager.create_profile(profile_name, "gate.dc.smartproxy.com", "20000", "lat", "lng", "spa5aef9ac", "Swqa1234+")  # create profile browser
            print("response here ", profile_response['uuid'])
            print("707070707070707")
            # save profile browser info in the database
            profile_browser_info = ProfileInfo(profile_name=profile_name, profile_uuid=profile_response['uuid'], ip_address="ip_address", port=123, lat=00, lng=11)
            profile_browser_info.save()
            self.profile_id = profile_response['uuid']
            #self.profile_id = "86c4ca33-46d3-43a2-aadf-4a3f41866d0f"
            sleep(1)
            site_credential = SiteCredential(userId=userId, password=password, profile=profile_browser_info, site_url=siteUrl)
            site_credential.save()

#########################################################################################
            print("ddddddddddddeeeerereresdfds")
            self.startProfile()
            sleep(10)
            (status, billDate, balance) = self.initialConnection(userId, password, siteUrl)

            print("phone numberssssss", billDate)
            return (status, billDate, balance)

        ################################### End ############

    def signout(self):
        try:
            sleep(2)
            account_button = self.driver.find_element_by_xpath('//*[@id="z1-profile-open"]/span[2]')
            account_button.click()
            sleep(2)
            print("162162162162")
            signout_button = self.driver.find_element_by_xpath('//*[@id="profile-signout-desktop"]')
            signout_button.click()
            print("wowthis is goodreat")
            self.driver.quit()
        except:
            print("signout eorrororor")
        
    def initialConnection(self, userId, password, siteUrl):
        try:
            sleep(6)
            self.driver.get(siteUrl)
            sleep(7)
            account_button = self.driver.find_element_by_xpath('//*[@id="z1-profile-open"]/span')
            account_button.click()
            sleep(2)
            print("67676767676767")
            signin_item = self.driver.find_element_by_xpath('//*[@id="profile-unauth"]/div[2]/div/ul/li[1]/a')
            signin_item.click()
            sleep(14)
            print("7070707070707070707070")
            username = self.driver.find_element_by_xpath('//*[@id="userName"]') 
            print("1571567")
            sleep(1)
            username.send_keys(userId)
            sleep(2)
            pwd = self.driver.find_element_by_xpath('//*[@id="password"]')
            pwd.send_keys(password)
            sleep(2)
            signin_button = self.driver.find_element_by_xpath('//*[@id="loginButton-lgwgLoginButton"]')
            signin_button.click()
            sleep(5)
            print("209029209202920292029")
######################## Wrong credentials handling #################
            try:
                pwd = self.driver.find_element_by_xpath('//*[@id="password"]')
                self.removeProfileSiteCredential(userId)
                return (INVALID_CREDENTIAL, "", "")
            except:
####################### Scrape numbers and send front-end ##########
                res = self.getPhoneNumber(userId)
                uuid = self.profile_id
                return ("phoneNumber", res, uuid)
#####################################################################
        except:
            self.removeProfileSiteCredential(userId)
            print("eror")
            return (INITIALCONNECTION_ERROR, "", "")

    def removeProfileSiteCredential(self, userId):
        ############### delete in the database ###############
        site_credential = SiteCredential.objects.filter(userId=userId)[0]
        profile_browser_info = site_credential.profile
        site_credential.delete()
        profile_browser_info.delete()
        ###################### End ############

    def refreshConnection(self, userId, password, siteUrl):
        try:
            self.driver.get(siteUrl)
            sleep(7)
            account_button = self.driver.find_element_by_xpath('//*[@id="z1-profile-open"]/span')
            account_button.click()
            sleep(2)
            print("67676767676767")
            signin_item = self.driver.find_element_by_xpath('//*[@id="profile-unauth"]/div[2]/div/ul/li[1]/a')
            signin_item.click()
            sleep(15)
            print("7070707070707070707070")
            username = self.driver.find_element_by_xpath('//*[@id="mobileSavedUserIdList"]/a')
            username.click()
            sleep(3)
            pwd = self.driver.find_element_by_xpath('//*[@id="password"]')
            pwd.send_keys(password)
            sleep(3)
            print("281828281828182818281")
            try:
                print("1919191919191919191")
                signin_button = self.driver.find_element_by_xpath('//*[@id="loginButton-lgwgLgBtMobile"]')
                signin_button.click()
                sleep(4)
                (status, billDate, balance) = self.getBalance()
                sleep(6)
                self.signout()
                sleep(5)
                return (status, billDate, balance)
            except:
                print("invalidinvliadinvliad")
                return (INVALID_PWD, "", "")
        except: # ask initial connection
            self.handleAskInitialConnection(self.profile_id,userId)
            print("refresh connecs erroror")
            return (REFRESHCONNECTION_ERROR, "", "")
            
    def handleAskInitialConnection(self, uuid, userId):
        self.removeProfileSiteCredential(userId)
        ##################################### remove profile browser ###############
        ml = MultiloginManager()
        ml.remove_profile(uuid)
        ############################## End #############

    def getPhoneNumber(self, userId):
        sleep(2)
        try:
            print("phone number here")
            phone_numbers = self.driver.find_elements_by_tag_name('label')
            num_array = []
            i = 1
            print("2626262626262626")
            for num in phone_numbers:
                res = {}
                print("this is what do you wnat", num)
                res["id"] = "phone" + str(i)
                i = i + 1
                res["label"] = num.text
                num_array.append(res)
            print(f"here are phone numbers: {phone_numbers}")
            return num_array
        except:
            self.removeProfileSiteCredential(userId)
            print("getPhoneNumber")
            return GET_PHONENUMBER_ERROR

    def getBalance(self):
        sleep(11)
        try:
            print("getBalance here")
            payDate_element = self.driver.find_element_by_xpath('//*[@id="myBilling"]/div[1]/div[2]/div[1]')
            sleep(2)
            print("306306")
            payDate = payDate_element.text
            print(payDate)
            sleep(1)
            print("dfdsafdasfdasfd")
            balance_element_integer = self.driver.find_element_by_xpath('//*[@id="myBilling"]/div[1]/div[1]/div[2]/span/span[2]/span[2]')
            balance_element_decimal = self.driver.find_element_by_xpath('//*[@id="myBilling"]/div[1]/div[1]/div[2]/span/span[2]/span[3]')
            sleep(1)
            balance = balance_element_integer.text + balance_element_decimal.text
            return (GET_BALANCE_SUCCESS, payDate, balance)
        except:
            print("getBalance error")
            return (BALANCE_ERROR, "","")

    def initialConnectionFromOtp(self, uuid, code):
        try:           
            print("here is otp")
            print(code)
            self.profile_id = uuid
            profile_info = ProfileInfo.objects.filter(profile_uuid=self.profile_id)[0]
            print("374374")
            self.driver = webdriver.Remote(command_executor=profile_info.profile_json, desired_capabilities={})
            sleep(2)
            print("372372237327372")
            code_input = self.driver.find_element_by_xpath('//*[@id="verificationCodeInput"]')
            code_input.send_keys(code)
            sleep(3)
            submit_button = self.driver.find_element_by_xpath('//*[@id="btnEnab"]/img')
            sleep(1)
            submit_button.click()
            print("wating next screen")
            sleep(13)
            delay = 13   #seconds
            try:
                myElem = WebDriverWait(self.driver, delay).until(EC.presence_of_element_located((By.ID, 'myBilling')))
                print("Page is ready!")
            except TimeoutException:
                self.removeProfileSiteCredential("4196511828")    # should use userId again.
                print("OTP page loading error")   
                return (OTP_LOADING_ERROR, "", "")  
            payDate_element = self.driver.find_element_by_xpath('//*[@id="myBilling"]/div[1]/div[2]/div[1]')  
            (temp,date,amount) = self.getBalance()
            print(temp)
            print(date)
            print(amount)
            print("herererdsadsadsadsassse")
            sleep(2)
            self.signout()
            sleep(1)
            return (temp, date, amount)
        except:
            self.removeProfileSiteCredential("4196511828")    # should use userId again.
            return (INVALID_OTP,"","")

    def initialConnectionPhoneNumber(self, uuid, which_phone_number):
        print("phone ndnndfndfd")
        print(which_phone_number)
        self.profile_id = uuid
        print(type(uuid))
        profile_info = ProfileInfo.objects.filter(profile_uuid=self.profile_id)[0]
        print("here")
        print(uuid)
        self.driver = webdriver.Remote(command_executor=profile_info.profile_json, desired_capabilities={})
        sleep(2)
        print("phonedfhdskhfd")
        
        phone_number_xpath = '//*[@id="2FAuthSelectOpts"]/div[2]/div/div/div['+which_phone_number+']'
        radio_button = self.driver.find_element_by_xpath(phone_number_xpath)
        radio_button.click()
        print("ddddsfsfasdfasfds")
        sleep(5)
        send_code_button = self.driver.find_element_by_xpath('//*[@id="btnEnab"]/img')
        send_code_button.click()
        sleep(10)
        try:
            code_input = self.driver.find_element_by_xpath('//*[@id="verificationCodeInput"]')
            return (OTP_CODE, uuid)
        except:
            self.removeProfileSiteCredential("4196511828")    # should use userId again.
            return (PHONE_NUMBER_LOADING_ERROR, "")
        


    



# ml = MultiLogin()
# #ml.login("4196511828","T54dbTF67!","4196511828")
# ml.test()