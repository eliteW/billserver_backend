import os
from django.db import models
from django.contrib.auth.models import User, Group
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone

# Create your models here.
class ProfileInfo(models.Model):
    profile_name = models.CharField(max_length=100, verbose_name=U'browser_profile')
    profile_uuid = models.CharField(max_length=100, blank=True, null=True)
    ip_address = models.CharField(max_length=50, blank=True, null=True)
    port = models.IntegerField(blank=True, null=True)
    lat = models.DecimalField(max_digits=11, decimal_places=8, blank=True, null=True)
    lng = models.DecimalField(max_digits=11, decimal_places=8,blank=True, null=True)
    profile_json = models.CharField(max_length=100, blank=True, null=True)

class SiteCredential(models.Model):
    userId = models.CharField(max_length=100, blank=True, null=True)
    password = models.CharField(max_length=100, blank=True, null=True)
    profile = models.ForeignKey(ProfileInfo, blank=True, null=True, verbose_name='RelatedProfile', on_delete=models.SET_NULL)
    site_url = models.CharField(max_length=100, blank=True, null=True)

class ProfileName(models.Model):
    lastProfileNumber = models.IntegerField(blank=True, null=True)

class UsernameVault(models.Model):
    username_key = models.CharField(max_length=100, blank=True, null=True)
    cipherUsername = models.CharField(max_length=100, blank=True, null=True)

class PasswordVault(models.Model):
    password_key = models.CharField(max_length=100, blank=True, null=True)
    cipherPassword = models.CharField(max_length=100, blank=True, null=True)

class CardInfoVault(models.Model):
    cardType = models.CharField(max_length=100, blank=True, null=True)
    keyCardNumber = models.CharField(max_length=100, blank=True, null=True)
    cipherCardNumber = models.CharField(max_length=100, blank=True, null=True)
    keyCardDate =  models.CharField(max_length=100, blank=True, null=True)
    cipherCardDate = models.CharField(max_length=100, blank=True, null=True)

class EncryptedInfoField(models.Model):
    site_url = models.CharField(max_length=100, blank=True, null=True)
    profile = models.ForeignKey(ProfileInfo, blank=True, null=True, verbose_name='RelatedProfile', on_delete=models.SET_NULL)
    userName = models.ForeignKey(UsernameVault, blank=True, null=True, on_delete=models.SET_NULL)
    password = models.ForeignKey(PasswordVault, blank=True, null=True, on_delete=models.SET_NULL)
    cardInfo = models.ForeignKey(CardInfoVault, blank=True, null=True, on_delete=models.SET_NULL)

class BillInfo(models.Model):
    site_name = models.CharField(max_length=100, blank=True, null=True)
    username = models.CharField(max_length=100, blank=True, null=True)
    password = models.CharField(max_length=100, blank=True, null=True)
    balance_amount = models.CharField(max_length=100, blank=True, null=True)
    balance_date = models.CharField(max_length=100, blank=True, null=True)
    last_refresh_date = models.DateTimeField('date joined', default=timezone.now)
    payment_amount = models.CharField(max_length=100, blank=True, null=True)
    last_pay_date = models.DateTimeField('date joined', default=timezone.now)
    status = models.BooleanField(default=True)
    profile = models.ForeignKey(ProfileInfo, blank=True, null=True, verbose_name='RelatedProfile', on_delete=models.SET_NULL)

