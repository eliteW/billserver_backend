from django.contrib import admin
from django.urls import path
from django.conf.urls import  include, url
from django.conf import settings
from django.template import RequestContext
from django import template
from django.template import Context
from rest_framework.schemas import get_schema_view
from dashboard import views
from multilogin import multilogin

urlpatterns = [
    url(r'^$', views.dashboard, name="dashboard"),
    url(r'^login/$', views.login, name="login"),
    url(r'^logout/$', views.logout, name="logout"),
    url(r'^biller/$', views.biller, name="biller"),
    url(r'^user/$', views.user, name="user"),
    url(r'^payment/$', views.payment, name="payment"),
]