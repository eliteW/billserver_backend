import os
import logging
from django.shortcuts import render, redirect
from django.template import RequestContext
from datetime import datetime, date
from django.contrib.auth.decorators import permission_required
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.decorators import login_required
from multilogin.models import (BillInfo, ProfileInfo, SiteCredential)

# Create your views here.

def login(request):
    if request.user.is_authenticated: # to avoid go to login screen again without logout.
        return redirect('dashboard')

    strMessage = ""
    if request.method=='POST':
        username = request.POST['email']
        password = request.POST['password']
        if __login(request, username, password):
            return redirect('dashboard')
        else:
            strMessage = "Username or password is invalid!"
    response = render(request,"signin.html",{ "message": strMessage} )
    return response

def __login(request, username, password):
    ret = False
    user = authenticate(username=username, password=password)
    if user:
        # if user.is_active and user.is_superuser:
        if user.is_active:
            auth_login(request, user)
            ret = True
    return ret


# sign out
def logout(request):
    auth_logout(request)
    return redirect('login')

@login_required
def dashboard(request):
    return render(request, "dashboard.html", {"test": "Hello"})

@login_required
def biller(request):
    return render(request, "biller.html", {"test": "Hello"})

@login_required
def user(request):
    return render(request, "user.html")

@login_required
def payment(request):
    bill_info = BillInfo.objects.all()
    return render(request, "payment.html", {"bill_payment_info": bill_info})


