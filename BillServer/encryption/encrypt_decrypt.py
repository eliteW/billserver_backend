import os
import base64
import hvac
client = hvac.Client(url='http://localhost:8200')
client.token = "s.4usAaitJurdudL3rrQEBxeYx"
print(client)
print(client.is_authenticated())

def transit_encrypt(plain_text, encrypt_key):
    """Encrypt plain text data with Transit.
    Keyword arguments:
    plain_text  -- the text to encrypt (string)
    encrypt_key -- encryption key to use (string)
    Return:
    ciphertext (string)
    """
    print("here is transit_encrypt")
    encoded_text = base64.b64encode(plain_text.encode("utf-8"))

    ciphertext = client.secrets.transit.encrypt_data(
        name = encrypt_key,
        plaintext = str(encoded_text, "utf-8")
    )

    return ciphertext["data"]["ciphertext"]

def transit_decrypt(ciphertext, decrypt_key):
    """Decrypt ciphertext into plain text.
    Keyword arguments:
    ciphertext -- the text to decrypt (string)
    decrypt_key -- decryption key to use (string)
    Return:
    Base64 decoded plaintext
    """
    decrypt_data_response = client.secrets.transit.decrypt_data(
        name = decrypt_key,
        ciphertext = ciphertext
    )

    return str(base64.b64decode(decrypt_data_response["data"]["plaintext"]), "utf-8")


# encoded_text = base64.b64encode("goodboy".encode("utf-8"))

# ciphertext = client.secrets.transit.encrypt_data(
#     name = "username",
#     plaintext = str(encoded_text, "utf-8")
# )

# print(ciphertext["data"]["ciphertext"])
# str_ciphertext = ciphertext["data"]["ciphertext"]

# decrypt_data_response = client.secrets.transit.decrypt_data(
#     name = "username",
#     ciphertext = str_ciphertext
# )

# decrypted = str(base64.b64decode(decrypt_data_response["data"]["plaintext"]), "utf-8")
# print(decrypted)